module.exports = {
  base: process.env.NODE_ENV == 'development' ? '/' : '/wenmu_blog/',
  title: '文木的笔记', // 网站的标题
  description: '文木前端的学习笔记', // 网站的描述信息
  head: [['link', { rel: 'icon', href: `/imgs3.webp` }]], // header 的图标
  themeConfig: {
    // navbar:false , // 禁用导航栏
    logo: '/imgs2.webp',
    search: true, // 是否使用导航栏上的搜索框
    searchMaxSuggestions: 5, // 搜索框显示的搜索结果数量
    lastUpdated: 'Last Updated', // string | boolean
    // 导航栏
    nav: [
      { text: 'Home', link: '/' },
      // 可指定链接跳转模式：默认target: '_blank'新窗口打开，_self当前窗口打开
      { text: 'Vue', link: '/menus/vue/' },
      { text: 'Css', link: '/menus/css/' },
      { text: 'Js', link: '/menus/js/' },
      {
        text: 'Webpack',
        link: '/menus/webpack/',
        // 子菜单
        // items: [
        //   { text: 'WebPack4', link: '#' },
        //   { text: 'WebPack5', link: '#' },
        // ],
      },
      { text: '小程序', link: '/menus/wx/' },
    ],

    // 侧边栏
    // sidebar: 'auto', // auto 自动补充侧边栏 false 禁用侧边栏

    sidebar: {
      // vue
      '/menus/vue/': [
        ['', '前端0'],
        ['testOne', '前端1'],
        ['testTwo', '前端2'],
      ],
      // js
      '/menus/js/': [
        ['', 'Js 0'],
        ['testOne', 'Js 1'],
        ['testTwo', 'Js 2'],
      ],
      // css
      '/menus/css/': [
        ['', 'Css 0'],
        ['testOne', 'Css 1'],
        ['testTwo', 'Css 2'],
      ],
      // webpack
      '/menus/webpack/': [
        ['', '标题0'],
        ['testOne', '标题1'],
        ['testTwo', '标题2'],
      ],
      // 小程序
      '/menus/wx/': [
        ['common', '常用组件'],
        ['fileDown', '文件下载'],
        ['fileUpload', '文件上传'],
        ['subpackage', '分包'],
        ['', '全局404页面'],
        ['devPackage', '常用依赖包'],
      ],
    },
    sidebarDepth: 2,
  },
};
