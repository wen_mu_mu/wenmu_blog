> 手机校验规则

```js
const checkPhone = (rule, value, callback) => {
  if (!/^((0\d{2,3}-\d{7,8})|(1[3456789]\d{9}))$/.test(value)) {
    callback(new Error('请输入正确的手机号或者固话号'));
  } else {
    callback();
  }
};
```
