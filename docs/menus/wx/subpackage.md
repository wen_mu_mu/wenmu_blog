# 小程序分包

> 在 page.js 中 pagesw 文件 同级下新建 subPackages 文件

```js

"subPackages":[{
    "root":"subPackages文件夹名称",
    "pages":[{
        "path":"路径",
        "style":{
        }
    },{
         "...内容同 pages 一致"
    }]
}]

// 预下载分包设置

"preloadRule":{
    'pages/index':{
        "network":"all",
        "packages":['activities']
    }
}

```
