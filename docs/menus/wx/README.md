# 全局 404 页面

> 在 App.vue 中 配置 onPageNotFound(页面不存在监听函数)

```js
onPageNotFound(){
    // navigateTo 跳转页面
    // switchTab 跳转tabber
    // redirectTo 重定向
    // navigateBack 页面返回
    // reLaunch 重加载
    uni.navigateTo({
        url:"/404 页面路径"
    })
}
```
