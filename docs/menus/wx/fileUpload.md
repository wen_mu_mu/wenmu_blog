```js

// 获取上传的文件信息

getFileInfo(file){ // file 使用时需看内容是什么
    let fileList = []
    for(let i=0;i<file.length,i++){
        fileList.push({
            id:file[i].id,
            url:file[i].url,
            originalName:file[i].name,
        })
    }
    uni.showLoading({
        title:"文件正在上传"
    })
    // 拿到 文件集合 遍历上传（一个一个上传)
    fileList.map(file=>{
        this.uploadFilePromise(file.url,file.originalName).then(res=>{
            // 此处看后台返回的数据 (此时是文件的线上地址和id)
            let {data:{id,url}} = JSON.parse(res)
            uni.hideLoading()
        })
    })
}




// url 文件路径 name 文件名称
uploadFilePromise(url,name){
    return new Promise((resolve,reject)=>{
        uni.uploadFile({
            url:baseUrl + api +`?originalName=${name}`,
            filePath:url,
            name:'file',
            success:(res)=>{
                setTimeout(()=>{
                    resolve(res)
                },1000)
            }
        })
    })
}
```
