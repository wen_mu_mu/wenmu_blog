# 文件下载

```js
// 使用
useDownLoadFile(file){
    uni.showLoading({
        title:'文件下载中',
    })
    //  下载
    this.downLoadFile(file.url,file.name)
    .then(res=>{
        // 打开
        this.openFile(res)

    }).catch(error=>{
        uni.showToast({
            title: '下载失败',
            icon: 'none'
        });
    }).finally(fiy=>{
        uni.hideLoading()
    })
},


// 下载
downLoadFile(url,name){
    let that = this
    return new Promise((resolve,reject)=>{
        uni.downloadFile({
            url:baseURL + url, // 线上下载地址
            success:(res=>{
                if(res.status == 200){
                    uni.saveFile({
                        tempFilePath:res.tempFilePath,   // 本地地址
                        filePath:wx.env.USER_DATA_PATH + '/' + name, // 微信缓存的 持久地址
                        success:({savedFilePath})=>{
                            resolve(savedFilePath); // 本地地址
                        }
                    })
                }
            })
        })
    })
}

// 打开
openFile(filePath){
    let system = uni.getSystemInfoSync().platform // 获取设备信息
    if(system == 'ios'){
        filePath = encodeURL(filePath)
    }
    uni.openDocument({
        showMenu:true,
        filePath,
        success:res=>{
            console.log('打开文档成功');
        },
        fail:err=>{
            uni.getImageInfo({
                scr:filePath,
                success:info=>({
                    current: filePath,
				    urls: [filePath]
                })
                fail: error => {
					uni.showToast({
					    title: '不支持该格式',
					    icon: 'none'
					});
					return;
				}
            })
        }
    })
}




```
