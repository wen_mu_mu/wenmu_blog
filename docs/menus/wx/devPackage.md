# 开发依赖包

> [uview2 ui 框架](https://www.uviewui.com/)

> [mescroll-uni 列表滚动](http://www.mescroll.com/)

> vuex-persistedstate 持久化缓存

```js
// vuex-persistedstate 配置 index.js 文件
import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
// 获取 store 文件下 modules 文件下的 所有 .js 文件
const files = require.context('./modules', false, /.js$/);
const modules = {};
files.keys().forEach((key) => {
  modules[key.replace(/(\.js|\.\/)/g, '')] = files(key).default;
});

Vue.use(Vuex);

const store = new Vuex.Store({
  modules,
  plugins: [
    createPersistedState({
      key: 'vuex_user_admin',
      storage: {
        getItem: (key) => uni.getStorageSync(key),
        setItem: (key, value) => uni.setStorageSync(key, value),
        removeItem: (key) => uni.removeStorageSync(key),
      },
      reducer(state) {
        return {
          user: {
            id: state.user.id,
          },
        };
      },
    }),
  ],
});
export default store;
```
