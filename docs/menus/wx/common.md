# uniApp 常见问题

> 样式不隔离 解决方案

```js

// 与data 同级
// 默认值 isolated(启动隔离)
// apply-shared(页面 wxss 样式将影响到自定义组件，但自定义组件 wxss 中指定的样式不会影响页面)
// shared(wxss 样式将影响到自定义组件，自定义组件 wxss 中指定的样式也会影响页面和其他设置了 apply-shared 或 shared 的自定义组件)、
options: {
    styleIsolation: 'shared',
}


```

> 组件式导航

```js
<navigatoe url="/路径" open-type="navigate">
  {' '}
  组件式导航{' '}
</navigatoe>
```

> 获取当前页面栈

```js

onLoad(){
    let route = getCurrentPages()
    console.log( '当前路由信息--->',route )
}

```

> 路由传参与接收

```js

// 跳转页面传参
uni.navigateTo({
    url:"page2?name='张三'&age='20'"
})


// 获取参数
onLoad(option){
    console.log(option.name,option.age)
}

**对于不规范字符格建议使用 encodeURL decodeURL 进行处理后传递**

```
