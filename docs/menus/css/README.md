

### 常用的样式

> 单行超出显示省略

```css
.oneline {
  overflow: hidden;
  word-break: break-all;
  text-overflow: ellipsis;
  -webkit-box-orient: horizontal;
  -webkit-line-clamp: 1;
  line-clamp: 1;
  display: -webkit-box;
}
```

> 多行超出显示省略

```css
.twoline {
  overflow: hidden;
  word-break: break-all;
  text-overflow: ellipsis;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
  line-clamp: 2;
  display: -webkit-box;
}
```
